package fr.gopartner.Repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import fr.gopartner.entities.Patient;

public interface PatientRepository extends JpaRepository<Patient, Long> {

	List<Patient> findByMalade(boolean m);

	// Page<Patient> findByMalade(boolean m, Pageable page);

	List<Patient> findByMaladeAndScoreLessThan(boolean m, int score);

	List<Patient> findByMaladeIsTrueAndScoreLessThan(int score);

	List<Patient> findByDateNaissanceBetween(Date d1, Date d2);

	List<Patient> findByDateNaissanceBetweenAndMaladeIsTrueOrNameLike(Date d1, Date d2, String mc);

	@Query("Select p From Patient p Where p.dateNaissance between :x and :y or p.name like :a ")
	List<Patient> cherchePatient(@Param("x") Date d1, @Param("y") Date d2, @Param("a") String mc);

}
