package fr.gopartner;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import fr.gopartner.Repositories.PatientRepository;
import fr.gopartner.entities.Patient;

@SpringBootApplication
public class PatientJpaSpringDataApplication implements CommandLineRunner {

	@Autowired
	private PatientRepository patientRepository;

	public static void main(String[] args) {
		SpringApplication.run(PatientJpaSpringDataApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println("**************** Bienvenue ***********");
		patientRepository.save(new Patient(null, "wajih", new Date(), false, 15));
		patientRepository.save(new Patient(null, "samir", new Date(), true, 96));
		patientRepository.save(new Patient(null, "karim", new Date(), true, 21));
		patientRepository.save(new Patient(null, "hajer", new Date(), false, 50));

		List<Patient> patients = patientRepository.findAll();
		patients.forEach(p -> {
			System.out.println("Le patient : " + p.getId() + " " + p.getName() + " " + p.getDateNaissance() + " "
					+ p.isMalade() + " " + p.getScore());
		});
		System.out.println("*****************************");
		Patient patient = patientRepository.findById(1L).orElseThrow(() -> new RuntimeException(("Patient not found")));
		if (patient != null)
			System.out.println("le patien de Id = 1 : " + patient.getName() + " est : malade = " + patient.isMalade());
		patient.setScore(890);
		System.out.println("Le patient : " + patient.getId() + " " + patient.getName() + " "
				+ patient.getDateNaissance() + " " + patient.isMalade() + " " + patient.getScore());

		System.out.println("==============================");

		List<Patient> byMalade = patientRepository.findByMalade(true);
		byMalade.forEach(p -> {
			System.out.println("le patient " + p.getName() + " est malade := " + p.isMalade());
		});

	}

}
